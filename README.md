# jsflow

Contrôle du flux d'instructions

# Exercice 1
Créer un script permettant d'afficher la valeur "null" dans la console, puis lors d'un clique sur un bouton afficher la valeur "true", "false", "true" et ainsi de suite.

# Exercice 2
Définir une variable avec la valeur 1, à chaque clique de l'utilisateur sur un bouton de l'interface cette valeur doit être incrémentée de 1. Ensuite, afficher dans la console si le nombre est "pair" ou "impair".

Lorsque le nombre est supérieur à 5 afficher, une alerte doit être lancée "stop tes cliques coco !!!"

# Exercice 3
Définir un tableau avec les valeurs suivantes : "simplon", "in code we trust", "jerome", "love", "symfony"

* "jerome" est là ?
* Où est "jerome" ?

# Exercice 4
Et hop, sans les mains ! Quels seront les résultats affichés dans la console ?
```
let a = 3;
let b = -2;

console.log(a > 0 && b > 0);

console.log(a > 0 || b > 0);

console.log(!(a > 0 || b > 0));
```
